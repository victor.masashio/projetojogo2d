﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{

    public int health = 10;
    public Slider healthBar;

    public float moveSpeed = 5;
    public float jumpForce = 1000;
    private int direction = 1; // 1 olhando pra direita, -1 pra esquerda
    
    public Transform groundCheck;
    public float groundCheckRadius = 0.1f;
    public bool grounded;
    public LayerMask whatIsGround;

    private Rigidbody2D rig;
    private Animator anim;
    private SpriteRenderer rend;

    private AudioSource death;
    private AudioSource shoot;
    private AudioSource shout;

    public GameObject FireballPrefab; // Objeto a ser criado
    public Transform firePositionRight; // Posicao sentido Direita
    public Transform firePositionLeft; // Posicao sentido Esquerda
    public float fireballSpeed;

    private bool fire;
    private float shootDelay = .5f;

    // Start is called before the first frame update
    void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        rend = GetComponent<SpriteRenderer>();
    }

                                                       
    void Update()
    {
        float h = Input.GetAxis("Horizontal");
        
        rig.velocity = new Vector2(h*moveSpeed, rig.velocity.y);
        anim.SetFloat("speed", Mathf.Abs(h));

        
        if (h > 0) {
            Flip(false);
        } else if (h < 0) {
            Flip(true);
        }
       

        // ver se o personagem esta no chão
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);

        // se eu estou no chão e pulei (barra de espaço)
        if(grounded && Input.GetButtonDown("Jump")) {
            // AddForce vai pular
            rig.AddForce(Vector2.up * jumpForce);
        } 
        // Se não estiver no chão, ativar animação de pula
        anim.SetBool("grounded", grounded);

        // Usa Fireball ao apertar 'E'
        if (Input.GetKeyDown(KeyCode.E)) {
            if (fire) return;
            fire = true;
            anim.Play("Player_Fireball");
            ShootFireball(rend.flipX);
            Invoke("ResetFireball", shootDelay);
        }



        healthBar.value = health;
    }
    
    void ShootFireball(bool flip)
    {
        // A cada tiro, vai criar um novo objeto 
        GameObject fireball;
        if (!flip) { // Se estiver olhando pra direita
            fireball = Instantiate(FireballPrefab, firePositionRight.position, Quaternion.identity); 
        } else { // Olhando pra esquerda
            fireball = Instantiate(FireballPrefab, firePositionLeft.position, Quaternion.identity);
            fireball.GetComponent<SpriteRenderer>().flipX = true;     
        }
        fireball.GetComponent<Rigidbody2D>().velocity = new Vector2(fireballSpeed * direction, 0);      
    }
    
    void ResetFireball()
    {
        fire = false;
        anim.Play("Player_Idle");
    }

    void Flip(bool f)
    {
        rend.flipX = f;
        direction = f? -1 : 1;
    }
    
    void OnTriggerEnter2D(Collider2D other) {
        
        if (other.gameObject.tag == "EnemyFire") {
            TakeDamage(1);
            Destroy(other.gameObject);
        }
    }
    
    void TakeDamage(int dmg) {
        health -= dmg;

        if(health <= 0) {
            anim.Play("Player_Death");
            // Metodo Invoke para dar um Delay até que o objeto seja destruido
            Invoke("Die", 1);
        }
    }

    void Die()
    {
        // Destruindo Player
        Destroy(this.gameObject);
    }
  
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    private float projectileLifeTime = 3;
    void Start()
    {
        Invoke("DestroyProjectile", projectileLifeTime);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void DestroyProjectile() {
        Destroy(this.gameObject);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float moveSpeed = 3;
    private int direction = 1; // 1 olhando pra direita, -1 pra esquerda


    //---------------------------------------

    private Rigidbody2D rig;
    private Animator anim;
    private SpriteRenderer rend;

    public int enemyHP = 3;

    public GameObject enemyFire;
    public Transform playerPosition;
    public float fireSpeed = 10;
    public Transform firePosition;

    private float shootDelay = .5f;
    private bool fire;

    // Start is called before the first frame update
    void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        rend = GetComponent<SpriteRenderer>();
    }
       
    void FixedUpdate()
    {
        rig.velocity = new Vector2(moveSpeed*direction, rig.velocity.y);

        float distance = Vector2.Distance(transform.position, playerPosition.position);

        if (distance <= 5) {
            //anim.SetTrigger("shoot");
            if (fire) return;
            fire = true;
            anim.Play("Enemy_Shoot");
            Shoot();
            Invoke("ResetShoot", shootDelay);
        }
    }
    
    void Shoot() {
        GameObject FireProj = Instantiate(enemyFire, firePosition.position, Quaternion.identity);
        FireProj.GetComponent<Rigidbody2D>().velocity = new Vector2(fireSpeed*direction, 0);
    }
 
    void ResetShoot()
    {
        fire = false;
        anim.Play("Enemy_Walk");
    }
  
    void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Limiter") {
            direction *= -1;
            rend.flipX = !rend.flipX;
        }
        if (other.gameObject.tag == "Fireball") {
            TakeDamage(1);
            Destroy(other.gameObject);
        }
    }
    

    void TakeDamage(int dmg) {
        enemyHP -= dmg;
        if(enemyHP <= 0) {
            Destroy(gameObject);
        }
    }
}

# Liu Kang Gonna Crazy

## O JOGO 

Liu Kang é um guerreiro Shaolin que foi banido para um passado alternativo e utilizando de suas técnicas de luta e magias, ele deve lutar contra inimigos de cada universo para retornar ao universo de Earthrealm e encontrar Raiden para ajudá-lo a retornar ao seu tempo.

## COMANDOS

COMANDO | AÇÃO
---------|---------
A        | Mover para Esquerda
S        | Mover para Direita
E        | Atirar Fireball
SPACE    | Pular 

## AUTORES
* Victor Masashi
* Vinícius Canuto
